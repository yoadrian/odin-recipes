# Odin Recipes

My first project from The Odin Project course/ Fundamentals/ HTML Foundations.

Contains a main index page and several other pages linked from the hompage.

View the project's page here: https://yoadrian.gitlab.io/odin-recipes

## Acknowledgements

Images and recipe related text are taken from allrecipes.com